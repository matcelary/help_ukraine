# help_ukraine scrapper

Script to scrap google map data of various places.

## Configuration

Goto `map.py` file and edit those variables:

```
# List of positions of cities you will find in the URL address of the page you browse manually
ll_list = [
    "@59.9045195,30.2727187,11z",
    "@55.5807481,36.8251261,9z",
    "@48.6700797,44.2265265,10z",
    "@55.795183,48.793127,10z",
    "@56.6349544,47.7596581,11z",
    "@53.2799469,34.2068795,11z",
    "@51.7119242,36.0420498,11z",
    "@50.5894186,36.4330517,11z",
    "@52.4249312,30.8108269,11z",
    "@52.0879164,23.5628053,11z",
    "@54.7798112,31.7197842,10z",
    "@54.1512769,33.2487099,13z",
]

# List of keywords that we search for
keyword_list = [
    "hotel",
    "art",
    "cafe",
    "school",
    "theatre",
    "shop",
]

# When you open new incognito tab, you have to click "agree" text in your language to continue
TEXT_AGREE = 'Zgadzam się'

# If there is no website available for this place, google is showing this text. Script will skip those entries.
TEXT_NO_WEBSITE = "Dodaj witrynę"

# If there is no telephone available, google is showing this text. Script will skip those entries.
TEXT_NO_PHONE = "Dodaj numer telefonu tego miejsca"
```

## Running

You got to have python installed. Open a project folder in the terminal and run:
```
pip install -r requirements.txt
python map.py
```

Program should open a new browser, you can watch as it progresses.

## Comments

It was really written fast and can have some issues if google change something in their HTML tree. 

