from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
import time
from datetime import datetime
import math
import re
import numpy

profile = webdriver.FirefoxProfile()
profile.set_preference('intl.accept_languages', 'en-US, en')
DRIVER = webdriver.Firefox(firefox_profile=profile)
DRIVER.implicitly_wait(3)

ll_list = [
    "@59.9045195,30.2727187,11z",
    "@55.5807481,36.8251261,9z",
    "@48.6700797,44.2265265,10z",
    "@55.795183,48.793127,10z",
    "@56.6349544,47.7596581,11z",
    "@53.2799469,34.2068795,11z",
    "@51.7119242,36.0420498,11z",
    "@50.5894186,36.4330517,11z",
    "@52.4249312,30.8108269,11z",
    "@52.0879164,23.5628053,11z",
    "@54.7798112,31.7197842,10z",
    "@54.1512769,33.2487099,13z",
]
keyword_list = [
    "hotel",
    "art",
    "cafe",
    "school",
    "theatre",
    "shop",
]

TEXT_AGREE = 'Zgadzam się'
TEXT_NO_WEBSITE = "Dodaj witrynę"
TEXT_NO_PHONE = "Dodaj numer telefonu tego miejsca"

DRIVER.get("https://www.google.com/maps/search/fuck_putin")
DRIVER.find_element_by_xpath(f"//span[contains(text(),'{TEXT_AGREE}')]").click()
OUTPUT = open("scraped_data.txt", "a", encoding="utf-8")


def geo_parse(url):
    match = re.search("\@.*z", url)
    if match:
        x, y, z = match[0].lstrip("@").rstrip("z").split(",")
    else:
        x, y, z = numpy.nan, numpy.nan, numpy.nan
    return float(x), float(y), float(z)


def geo_distance(url_1, url_2):
    x1, y1, _ = geo_parse(url_1)
    x2, y2, _ = geo_parse(url_2)
    return math.sqrt(abs(x1 - x2) + abs(y1 - y2))


def check_distance(url_1, url_2, distance):
    if geo_parse(url_2)[2] < 9:
        return False
    if geo_distance(url_1, url_2) < distance:
        return True
    return False


def spam_click(element, timeout=5):
    """ Try to click until it passes. """
    t1 = datetime.now()
    while (datetime.now() - t1).seconds <= timeout:
        try:
            element.click()
            return True
        except:
            pass
    return False


def scroll_to_bottom(t=0.3):
    """ scrolls div until scrolling stops"""
    scrollbox = DRIVER.find_element_by_css_selector("div.section-scrollbox > div.section-scrollbox")
    while True:
        scroll_top = DRIVER.execute_script("return arguments[0].scrollTop", scrollbox)
        scroll_top_max = DRIVER.execute_script("return arguments[0].scrollTopMax", scrollbox)
        if scroll_top == scroll_top_max:
            break
        else:
            DRIVER.execute_script(f"arguments[0].scroll(0, {scroll_top_max});", scrollbox)
            time.sleep(t)
    time.sleep(t)
    return


def loading_wait():
    time.sleep(0.15)
    DRIVER.find_element_by_css_selector("#searchboxinput[aria-label='Wyszukaj w Mapach Google']")
    time.sleep(4)


def get_links(url):
    DRIVER.get(url)
    links = []
    # check if google didn't zoom out maliciously
    while check_distance(ll, DRIVER.current_url, 0.2):
        scroll_to_bottom()
        anchors = DRIVER.find_elements_by_css_selector("a.a4gq8e-aVTXAb-haAclf-jRmmHf-hSRGPd")
        for anchor in anchors:
            links.append(anchor.get_attribute("href"))
        next_page = DRIVER.find_element_by_css_selector("button[jsaction='pane.paginationSection.nextPage']")
        clicked = spam_click(next_page)
        if not clicked:
            break
        loading_wait()
    return links


def scrap_info(link):
    DRIVER.get(link)
    info_div = DRIVER.find_element_by_css_selector("div[jsan='t-dgE5uNmzjiE,7.siAUzd-neVct,0.aria-label,0.role']")
    website = info_div.find_element_by_xpath("//img[@src='//www.gstatic.com/images/icons/material/system_gm/2x/public_gm_blue_24dp.png']/../../..").text\
        .replace(TEXT_NO_WEBSITE, "")
    phone = info_div.find_element_by_xpath("//img[@src='//www.gstatic.com/images/icons/material/system_gm/2x/phone_gm_blue_24dp.png']/../../..").text\
        .replace(TEXT_NO_PHONE, "")
    name = DRIVER.find_element_by_css_selector("h1[class='x3AX1-LfntMc-header-title-title gm2-headline-5']").text
    return [name, phone, website]


start = datetime.now()

if __name__ == "__main__":
    for ll in ll_list:
        for keyword in keyword_list:
            url = f"https://www.google.com/maps/search/{keyword}/{ll}"
            links = get_links(url)
            for link in links:
                try:
                    info = [keyword] + scrap_info(link) + [link]
                    # we should check if really the same place, but whatever
                    line = "\t".join(info)
                    OUTPUT.write(line + "\n")
                    OUTPUT.flush()
                except:
                    pass

OUTPUT.close()
DRIVER.quit()

print(datetime.now() - start)